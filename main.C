//n 2 dimensional spheres colliding

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <iostream>
#include <string.h>
#include <vector>
#include <GL/glut.h>
#include <GL/freeglut.h>
#include <algorithm>
using namespace std;

#define pi 3.141592654
float trun     = 100 ;     //initial running time
float dt       = 0.001;    //initial step
float r        = 0.02;     //initial radius
int   n        = 200;      //initial number of particles
int   k        = 0;        //iterator
int iterations = (int)(trun/dt)+trun;
int speed      = 2;
int bins       = 0;

vector <float> x(n,0), y(n,0), vx(n,0), vy(n,0);
vector <float> t_x_y_vx_vy(1+4*n); //a vector with time, positions and velocities
vector<vector<float> > evol (iterations, vector<float>(1+4*n));
vector<vector<float> > meanvelocity;
vector <float> statevol(iterations,0);

vector<float> aux1(n,0);
vector<vector<float> > mean(iterations,vector<float>(1+n));
vector<float> aux2(n,0);
vector<vector<float> > var(iterations,vector<float>(1+n));
vector<float> aux3(n,0);
vector<vector<float> > vmean(iterations,vector<float>(1+n));
vector<vector<float> > dist(iterations,vector<float>(1+n));
 
int iteratorGL = 0;
int maxelement = 0;
float maxvelocity = 0;

char title[]      = "N particles in a 2D box";
char graphtitle[] = "Graph\n1";
char printtime[10];

//char xaxis[]      = "x axis";
//char yaxis[]      = "y axis";
int windowWidth   = 700;
int windowHeight  = 700;
int windowPosX    = 0;
int windowPosY    = 0;


GLdouble xLeft, xRight, yBottom, yTop;


int refreshMillis = 30;



//prototypes
bool test(vector <float> x, vector <float> y, int i);
void TimeWalls(vector <float> x, vector <float> y, vector <float> vx, vector <float> vy, int n, float r, float *TimeWallColl,int *n1, int *n2);
void TimeParticleColl( vector <float> x,vector <float> y,vector <float> vx, vector <float> vy, int n, float r, float *TimePartColl, float *angle, int *n3, int *n4);
void WallColl(vector <float>* vx,vector <float>* vy, int n1, int n2);
void ParticleColl(vector <float>* vx,vector <float>* vy, float angle, int n3, int n4);
void dynamics(vector <float>* x,vector <float>* y, vector <float> vx,vector <float> vy, int n, float dt1);
void cm(float *x,float *y, int n, float *cmx, float *cmy);
void initGL();
void display();
void idle();
void reshape(GLsizei weight, GLsizei height);
void Timer(int value);
void initGL(); 
void draw(float scale);
void print(int x, int y,int z, char *string);
void display();
void reshape(GLsizei weight, GLsizei height);
void Timer(int value);





//particle superposion test
bool test(vector <float> x,vector <float> y, int i){
  for(int j=0; j<i;j++){
    if((x[j]-x[i])*(x[j]-x[i])+(y[j]-y[i])*(y[j]-y[i])<=4.0*r*r){
      return false;}
  }
  return true;
}


//calculation of times of collision with the walls
void TimeWalls(vector <float> x, vector <float> y, vector <float> vx, vector <float> vy, int n, float r, float *TimeWallColl,int *n1, int *n2){
  float t1, t2, t3, t4;
  float t5, t6, t7, t8;
  float x1, x2, x3, x4;
  float y1, y2, y3, y4;

  int i;

  //collisions with the walls
  //wall 1: x=0;
  t1=(r-x[0])/vx[0]; //time till collision with wall 1
  x1=0.0;            //collision position x
  y1=y[0]+t1*vy[0];  //collision position y

  //same, for the other 3 walls
  t2=(1.0-r-y[0])/vy[0]; y2=1.0; x2=x[0]+t2*vx[0] ;
  t3=(1.0-r-x[0])/vx[0]; x3=1.0; y3=y[0]+t3*vy[0];
  t4=(r-y[0])/vy[0];     y4=0.0; x4=x[0]+t4*vx[0];

  //corners 5, 6, 7, 8
  t5=t6=(r-x[0])/vx[0];
  t7=t8=(1.0-r-x[0])/vx[0];

  //for particle '0'
  //collision with wall 1
  if(vx[0]<0 && t1>0 && y1>0 && y1<1){ if(t1 < *TimeWallColl){*TimeWallColl=t1; *n1=0;*n2=1;};};
  //collision with wall 2
  if(vy[0]>0 && t2>0 && x2>0 && x2<1){ if(t2 < *TimeWallColl){*TimeWallColl=t2; *n1=0;*n2=2;};};
  //collision with wall 3
  if(vx[0]>0 && t3>0 && y3>0 && y3<1){ if(t3 < *TimeWallColl){*TimeWallColl=t3; *n1=0;*n2=3;};};
  //collision with wall 4
  if(vy[0]<0 && t4>0 && x4>0 && x4<1){ if(t4 < *TimeWallColl){*TimeWallColl=t4; *n1=0;*n2=4;};};
  //collision with the corners
  if(vx[0]<0 && vy[0]<0 && x[0]==y[0]&& vx[0]==vy[0] && t5>0){ if(t5 < *TimeWallColl){*TimeWallColl=t5; *n1=0;*n2=5;};};
  if(vx[0]<0 && vy[0]>0 && x[0]==1.0-y[0]&& vx[0]==-vy[0] && t6>0){ if(t6 < *TimeWallColl){*TimeWallColl=t6; *n1=0;*n2=6;};};
  if(vx[0]>0 && vy[0]>0 && x[0]==y[0]&& vx[0]==vy[0] && t7>0){ if(t7 < *TimeWallColl){*TimeWallColl=t7; *n1=0;*n2=7;};};
  if(vx[0]>0 && vy[0]<0 && x[0]==1.0-y[0]&& vx[0]==-vy[0] && t8>0){ if(t8 < *TimeWallColl){*TimeWallColl=t8; *n1=0;*n2=8;};};

  //same with all the other particles
  for(i=1;i<n;i++){
    t1=(-x[i]+r)/vx[i]; x1=0.0; y1=y[i]+t1*vy[i];
    t2=(1.0-r-y[i])/vy[i]; y2=1.0; x2=x[i]+t2*vx[i] ;
    t3=(1.0-r-x[i])/vx[i]; x3=1.0; y3=y[i]+t3*vy[i];
    t4=(r-y[i])/vy[i]; y4=0.0; x4=x[i]+t4*vx[i];
    t5=(-x[i]+r)/vx[i]; t6=(-x[i]+r)/vx[i];t7=(1.0-r-x[i])/vx[i];t8=(1.0-r-x[i])/vx[i];
    if(vx[i]<0 && t1>0 && y1>0 && y1<1){ if(t1 < *TimeWallColl){*TimeWallColl=t1; *n1=i;*n2=1;};};
    if(vy[i]>0 && t2>0 && x2>0 && x2<1){ if(t2 < *TimeWallColl){*TimeWallColl=t2; *n1=i;*n2=2;};};
    if(vx[i]>0 && t3>0 && y3>0 && y3<1){ if(t3 < *TimeWallColl){*TimeWallColl=t3; *n1=i;*n2=3;};};
    if(vy[i]<0 && t4>0 && x4>0 && x4<1){ if(t4 < *TimeWallColl){*TimeWallColl=t4; *n1=i;*n2=4;};};
    if(vx[i]<0 && vy[i]<0 && x[i]==y[i]&& vx[i]==vy[i] && t5>0){ if(t5 < *TimeWallColl){*TimeWallColl=t5; *n1=0;*n2=5;};};
    if(vx[i]<0 && vy[i]>0 && x[i]==1.0-y[i]&& vx[i]==-vy[i] && t6>0){ if(t6 < *TimeWallColl){*TimeWallColl=t6; *n1=0;*n2=6;};};
    if(vx[i]>0 && vy[i]>0 && x[i]==y[i]&& vx[i]==vy[i] && t7>0){ if(t7 < *TimeWallColl){*TimeWallColl=t7; *n1=0;*n2=7;};};
    if(vx[i]>0 && vy[i]<0 && x[i]==1.0-y[i]&& vx[i]==-vy[i] && t8>0){ if(t8 < *TimeWallColl){*TimeWallColl=t8; *n1=0;*n2=8;};};
  }

  return;
}


//calculation of times of collision of particles with each other
void TimeParticleColl( vector <float> x,vector <float> y,vector <float> vx, vector <float> vy, int n, float r, float *TimePartColl, float *angle, int *n3, int *n4){

  float radical;
  float t1;
  float x1aux, x2aux, y1aux, y2aux;
  float a, b, c;
  //??
  for(int i = 0;i<n;i++){
    for(int j = i+1;j<n;j++){
      a = (vx[j]-vx[i])*(vx[j]-vx[i])+(vy[j]-vy[i])*(vy[j]-vy[i]);
      b = 2.0*(x[j]-x[i])*(vx[j]-vx[i])+2.0*(y[j]-y[i])*(vy[j]-vy[i]);
      c = (x[j]-x[i])*(x[j]-x[i])+(y[j]-y[i])*(y[j]-y[i])-4.0*r*r;
      //c = (x[j]-x[i])*(x[j]-x[i])+(y[j]-y[i])*(y[j]-y[i])-r*r;
      radical = b*b-4*a*c;
      if(radical > 0.0 && b < 0.0){
	t1    = -0.5*(b+sqrt(radical))/a;
	x1aux = x[i]+vx[i]*t1;
	y1aux = y[i]+vy[i]*t1;
	x2aux = x[j]+vx[j]*t1;
	y2aux = y[j]+vy[j]*t1;

	if(x1aux>r &&
	   x1aux<1.0-r &&
	   y1aux>r &&
	   y1aux<1.0-r &&
	   x2aux>r &&
	   x2aux<1.0-r &&
	   y2aux>r &&
	   y1aux<1.0-r){
	  if(t1<*TimePartColl){
	    *TimePartColl = t1;
	    *angle = atan2(y[j]-y[i]+t1*(vy[j]-vy[i]) , x[j]-x[i]+t1*(vx[j]-vx[i]));
	    if(y[j]-y[i]+t1*(vy[j]-vy[i])<0){
	      *angle = (float)2.*pi+*angle;
	    }
	    *n3 = i;
	    *n4 = j;
	  }
	}
      }
    }
  }
  return;
}



//a step in time...
void dynamics(vector<float>* x,vector<float>* y, vector <float> vx,vector <float> vy, int n, float dt1){
  //simple time evolution
  for(int i=0;i<n;i++){
    (*x)[i]=(*x)[i]+vx[i]*dt1;  
    (*y)[i]=(*y)[i]+vy[i]*dt1;
  }
  return;
}



//sign change of velocities after collision
void WallColl(vector <float>* vx,vector <float>* vy, int n1, int n2){

  if(n2==1){(*vx)[n1]=-(*vx)[n1];};
  if(n2==2){(*vy)[n1]=-(*vy)[n1];};
  if(n2==3){(*vx)[n1]=-(*vx)[n1];};
  if(n2==4){(*vy)[n1]=-(*vy)[n1];};
  if(n2==5){(*vx)[n1]=-(*vx)[n1]; (*vy)[n1]=-(*vy)[n1];};
  if(n2==6){(*vx)[n1]=-(*vx)[n1]; (*vy)[n1]=-(*vy)[n1];};
  if(n2==7){(*vx)[n1]=-(*vx)[n1]; (*vy)[n1]=-(*vy)[n1];};
  if(n2==8){(*vx)[n1]=-(*vx)[n1]; (*vy)[n1]=-(*vy)[n1];};
  return;

}



//velocity change after elastic collision between particles
void ParticleColl(vector <float>* vx,vector <float>* vy, float angle, int n3, int n4){
  float v1xc, v1yc, v2xc, v2yc;
  float aux;

  v1xc = (*vx)[n3]*cos(angle)+(*vy)[n3]*sin(angle);
  v1yc = -(*vx)[n3]*sin(angle)+(*vy)[n3]*cos(angle);
  v2xc = (*vx)[n4]*cos(angle)+(*vy)[n4]*sin(angle);
  v2yc = -(*vx)[n4]*sin(angle)+(*vy)[n4]*cos(angle);
  
  if(v1xc>v2xc){aux=v1xc; v1xc=v2xc; v2xc=aux;}
  
  (*vx)[n3] = v1xc*cos(angle)-v1yc*sin(angle);
  (*vy)[n3] = v1xc*sin(angle)+v1yc*cos(angle);
  (*vx)[n4] = v2xc*cos(angle)-v2yc*sin(angle);
  (*vy)[n4] = v2xc*sin(angle)+v2yc*cos(angle);
  return;
}


void initGL(){
  glClearColor(1.0, 1.0, 1.0, 1.0); //black
}

void print(int x, int y,int z,char *string, int fontsize)
{
  //set the position of the text in the window using the x and y coordinates
  glRasterPos2f(x,y);
  //get the length of the string to display
  int len = (int) strlen(string);
    
  //loop to display character by character
  for (int i = 0; i < len; i++)
    {
      if(fontsize == 2){	
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24,string[i]);}
      else if (fontsize == 1){
	glutBitmapCharacter( GLUT_BITMAP_HELVETICA_18 ,string[i]);}

    }
};



//draw graphics
void drawhistogram(float scale)
{
         
  glPushMatrix(); //GL_MODELVIEW is default

  glScalef(scale / (bins), scale / maxelement,1);
  glColor3f(255, 0, 0);
  
  glLineWidth(0.2);

  glBegin(GL_LINE_STRIP);
  glVertex2f(0,0);

  for(int c = 0; c < bins; c++)
    {
      glVertex2f(c,meanvelocity[iteratorGL][c]);
      glVertex2f(c+1, meanvelocity[iteratorGL][c]);
      glVertex2f(c+1,0);
    }
         
  glEnd();
  glPopMatrix(); 
};


/*void draw(float scale)
{
  //float x, dx = 1.0/N; //N is the number of points with which we'll draw the function
  //x1, x2 (and y1, y2) are the limits of the function 
         
  glPushMatrix(); 
  //glScale — multiply the current matrix by a general scaling matrix
  //the arguments are the scale factors along x, y and z axes.
  glScalef(scale / (iterations), scale/2.0, 0);
  //glTranslatef(0.0, -1.0, 0.0);
  glColor3f(0, 0, 0);
  
  glLineWidth(0.1);

  glBegin(GL_LINE_STRIP);
  
  for(int c = 1; c < iteratorGL; c++)
    {
      glVertex2f(c, statevol[c]);
    }
         
  glEnd();
         
  glPopMatrix(); //problem?
  };*/



//display function for OpenGL
void display(){
  //scale relative to [0,1]x[0,1] in the coordinate system used bellow
  float scale = 0.8;


  //draw box
  glClear(GL_COLOR_BUFFER_BIT); //??
  
  glLoadIdentity();
  glLineWidth(2);
  glColor3f(0.9, 0.9, 0.97);

  glTranslatef(-1+(1-scale)/2,(1-scale)/2,0);

  
  glBegin(GL_QUADS);
  glVertex2f(0, 0);  // vertex positions? counter-clockwise
  glVertex2f(scale,0 );
  glVertex2f(scale,scale);
  glVertex2f(0,scale);
  glEnd();


  //draw particles
  glLoadIdentity();
  glTranslatef(-1+(1-scale)/2,(1-scale)/2,0);

  glTranslatef(scale*evol[iteratorGL][1],scale*evol[iteratorGL][1+n],0.0);

  
  glBegin(GL_TRIANGLE_FAN); //to form a circle
  glColor3f(1.0,0.0,0.0); //red
  glVertex2f(0.0,0.0); //center of the circle??
  int numSegments1 = 100;
  GLfloat angle1;
  for (int i=0; i<=numSegments1; i++){
    angle1 = i*2.0*pi/numSegments1;
    glVertex2f(cos(angle1)*r*scale,sin(angle1)*r*scale);}
  glEnd();

  
  int j;
  j = 1;
  while(j<n){
    glLoadIdentity();
    glTranslatef(-1+(1-scale)/2,(1-scale)/2,0);
    glTranslatef(scale*evol[iteratorGL][j+1],scale*evol[iteratorGL][j+1+n],0.0);

    glBegin(GL_TRIANGLE_FAN); //to form a circle
    glColor3f(0.0,0.0,1.0); //blue
    glVertex2f(0.0,0.0); //center of the circle??
    int numSegments2 = 100;
    GLfloat angle2;
    for (int i=0; i<=numSegments2; i++){
      angle2 = i*2.0*pi/numSegments2;
      glVertex2f(cos(angle2)*r*scale,sin(angle2)*r*scale);
    }
  
    glEnd();
    j++;
  }
  
  //else  glutLeaveMainLoop();

  //draw text
  glLoadIdentity();
  glTranslatef(0.4,0,0);
  glColor3ub(0, 0, 0); 
  print(0,0,0, graphtitle, 2);

  sprintf(printtime,"t=%d",iteratorGL);

  
  glLoadIdentity();
  glTranslatef(-0.05,-0.15,0);
  glColor3ub(0, 0, 0); 
  print(0,0,0, printtime, 1);

  //glLoadIdentity();
  //glTranslatef(0.77,-0.95,0);
  //glColor3ub(0, 0, 0); 
  //print(0,0,0, yaxis, 1);


  //glLoadIdentity();
  //glTranslatef(0.4,0.6,0);
  //glColor3ub(0,0,0);
  //print(0,0,0, numbpar, 1);
  //glTranslatef(0,-0.2,0);
  //print(0,0,0, parradi, 1);
  glLoadIdentity();
  glTranslatef((1-scale)/2, -1+((1-scale)/2), 0);

  //draw axis
  glLineWidth(0.8);
  glBegin(GL_LINE_STRIP);
  glVertex2f(0,scale+0.04);
  glVertex2f(0,0);
  glVertex2f(scale+0.04,0);
  glEnd();
  glBegin(GL_LINE_STRIP);
  glVertex2f(-0.01,scale+0.04-0.02);
  glVertex2f(0,scale+0.04);
  glVertex2f(0.01,scale+0.04-0.02);
  glEnd();
  glBegin(GL_LINE_STRIP);
  glVertex2f(scale+0.04-0.02, -0.01);
  glVertex2f(scale+0.04,0);
  glVertex2f(scale+0.04-0.02, 0.01);
  glEnd();

  //draw graph
  drawhistogram(scale);

  
  if(iteratorGL+speed>0 && iteratorGL+speed<iterations-10){
    iteratorGL=iteratorGL+speed;
  }
  
  glutSwapBuffers();
 
}

void reshape(GLsizei weight, GLsizei height){
  if(height == 0) height = 1;
  GLfloat aspect = (GLfloat) weight/height;

  glViewport(0,0,weight,height);

  glMatrixMode(GL_PROJECTION); //??
  glLoadIdentity();
  if(weight<=height){
    xLeft   = -1.0;
    xRight  = 1.0;
    yBottom = -1.0 /aspect;
    yTop    = 1.0/aspect;
  }
  else{
    xLeft   = -1.0*aspect;
    xRight  = 1.0*aspect;
    yBottom = -1.0;
    yTop    = 1.0;
  }

  gluOrtho2D(xLeft, xRight, yBottom, yTop);
 

  glMatrixMode(GL_MODELVIEW); // ??
  glLoadIdentity();
}

void Timer(int value){
  glutPostRedisplay();
  glutTimerFunc(refreshMillis, Timer,0);
}


/* Key press processing */


void key(unsigned char c, int x, int y)
{
  if(c == 27){
    iteratorGL = 0;
    glutLeaveMainLoop();
  }
};

void specialKeys(int key, int x, int y)
{		
  switch(key)
    {
    case GLUT_KEY_RIGHT:
      if(speed == -1){
	speed = 1; break;
      }
      if (speed >= 1){
      speed *= 2; break;
      }
      if (speed < 1){
	speed *= 0.5; break;
      }




		      
    case GLUT_KEY_LEFT:
      if (speed == 1)
      {
	speed *= -1; break;
      }
      if (speed > 1){
	speed *= 0.5; break;
      }
      if (speed < 1){
	speed *= 2; break;
      }

    }
};


int main (int argc, char **argv){
  //user
  while(true)
    {
  
  cout << "insert number of particles (default:n=100):\n";
  cin >> n;

  cout << "insert particle radius (default:r=0.03):\n";
  cin >> r;

  cout << "insert a step for calculations (default:dt=0.001):\n";
  cin >> dt;

  cout << "insert the running time (default:trun=50)\n";
  cin >> trun;
 
  
  iterations = (int)(trun/dt)+trun;
  x.resize(n);
  y.resize(n);
  vx.resize(n);
  vy.resize(n);
  t_x_y_vx_vy.resize(1+4*n);
  evol.resize(iterations,vector<float>(1+4*n));
  statevol.resize(iterations,0);
  bins = 0;
  maxelement = 0;
  maxvelocity = 0;
  
  aux1.resize(n,0);
  mean.resize(iterations,vector<float>(n));
  aux2.resize(n,0);
  var.resize(iterations,vector<float>(n));
  aux3.resize(n,0);
  vmean.resize(iterations,vector<float>(n));
  dist.resize(iterations,vector<float>(n));
 

  

  //sprintf(parradi, "radius        = %f",r);

  //boundaries
  float xmax, xmin, ymax, ymin;
  xmax = ymax = 1;
  xmin = ymin = 0;

  //collision times
  float TimeWallColl, TimePartColl;

  //angle between the tragectories of two colliding spheres
  float angle;

  //center of mass
  float cmx, cmy;

  //stats
  //float vmean, mean, var, dist;

  //time evoltuion
  float t;
  
  //auxiliar
  float dt1;

  //flags for collisions
  int n1; //particle lable
  int n2; //wall/corner lable
  int n3;
  int n4;

  //??
  int m;


      
    //randoms, seed
    srand(time(NULL));

    cout << "creating initial positions"<< "\n";
  
    //random (center) position for particle '0' within r and 1-r
    x[0] = r+(float)0.0001+((float)1.0-(float)2.0*(r+(float)0.0001))*(float)rand()/(float)RAND_MAX; 
    y[0] = r+(float)0.0001+((float)1.0-(float)2.0*(r+(float)0.0001))*(float)rand()/(float)RAND_MAX;

    //random velocity for particle '0' within -1 and 1
    vx[0] = -(float)1.0+(float)2.0*(float)rand()/(float)RAND_MAX;
    vy[0] = -(float)1.0+(float)2.0*(float)rand()/(float)RAND_MAX;

    //iterating over the n particles

    int i = 1;
    while(i<n)
      {
	x[i]  = r+(float)0.0001+((float)1.0-(float)2.0*(r+(float)0.0001))*(float)rand()/(float)RAND_MAX; 
	y[i]  = r+(float)0.0001+((float)1.0-(float)2.0*(r+(float)0.0001))*(float)rand()/(float)RAND_MAX;
	vx[i] = -(float)1.0+(float)2.0*(float)rand()/(float)RAND_MAX;
	vy[i] = -(float)1.0+(float)2.0*(float)rand()/(float)RAND_MAX;
	if (test(x,y,i)) {++i;}
      }
    
    //func(&x);
    //cout<<x[1]<< "\n";  
    
    //stats
    i = 0;
    m = 0;
    while(i<n)
      {
	aux1[0] = x[i]; //x position
	aux2[0] = (x[i]-mean[m][i])*(x[i]-mean[m][i]);//quadratic mean deviation
	aux3[0] = vx[i]*vx[i]+vy[i]*vy[i]; // quad vel
	++i;
      }

    cout << "time evolution"<< "\n";


    //initializing times
    t=0;
    dt1=dt;
  
    //time evolution
    int p=0;
    while(t<=trun){
      //initializing lables and times
      n1=n2=n3=n4=0;
      TimeWallColl = dt;
      TimePartColl = dt;

      //calculation of collision times
      TimeWalls(x, y, vx, vy, n, r, &TimeWallColl,&n1,&n2);
      TimeParticleColl(x, y, vx, vy,n,r,&TimePartColl,&angle,&n3,&n4);

      //if there are no collisions near a dt step is given
      if(TimeWallColl>dt1 && TimePartColl>dt1){
	dynamics(&x,&y,vx,vy,n,dt1);
	t+=dt;
	m+=1;
	i=0;
	while(i<n)
	  {
	    aux1[i] += x[i];//somam-se as posições e divide-se pelo numero de interações??
	    mean[m][i] = aux1[i]/(float)m;
	    aux2[i] += (x[i]-mean[m][i])*(x[i]-mean[m][i]);
	    var[m][i] = sqrt(aux2[i]/(float)m);
	    aux3[i] += vx[i]*vx[i]+vy[i]*vy[i];
	    vmean[m][i] = sqrt(aux3[i]/(float)m);
	    if(i!=n-1){
	      dist[m][i]=sqrt(pow(x[i]-x[i+1],2)+
			      pow(y[i]-y[i+1],2))-2.0*r;
	    }
	    ++i;
	  }
	dt1 = dt;
	t_x_y_vx_vy[0] = t; //t_x_y_vx_vy[it]?
	int i = 0;
	while (i<n){
	  t_x_y_vx_vy[i+1]=x[i];
	  t_x_y_vx_vy[i+1+n]=y[i];
	  t_x_y_vx_vy[i+1+2*n]=vx[i];
	  t_x_y_vx_vy[i+1+3*n]=vy[i];
	  ++i;
	}
	evol[m] = t_x_y_vx_vy;
	//statevol[m]=vmean;

	if((float)m/iterations>0.1 && p==0){
	  cout << "[=>                  ]10%" << "\n";
	  ++p;
	}
	if((float)m/iterations>0.2 && p==1){
	  cout << "[===>                ]20%" << "\n";
	  p++;
	}
	if((float)m/iterations>0.3 && p==2){
	  cout << "[=====>              ]30%" << "\n";
	  p++;
	}
	if((float)m/iterations>0.4 && p==3){
	  cout << "[=======>            ]40%" << "\n";
	  p++;
	}
	if((float)m/iterations>0.5 && p==4){
	  cout << "[=========>          ]50%" << "\n";
	  p++;
	}
	if((float)m/iterations>0.6 && p==5){
	  cout << "[===========>        ]60%" << "\n";
	  p++;
	}
	if((float)m/iterations>0.7 && p==6){
	  cout << "[=============>      ]70%" << "\n";
	  p++;
	}
	if((float)m/iterations>0.8 && p==7){
	  cout << "[===============>    ]80%" << "\n";
	  p++;
	}
	if((float)m/iterations>0.9 && p==8){
	  cout << "[=================>  ]90%" << "\n";
	  p++;
	}
	if((float)m/iterations>0.98 && p==9){
	  cout << "[===================>]100%" << "\n";
	  p++;
	}

      }
    
      //change of step if a collision is nearer than the step accounts
      //if the next collision is with a wall...
      else if (TimeWallColl<=dt1 && TimeWallColl<TimePartColl){
	dt1 = TimeWallColl;
	dynamics(&x,&y,vx,vy,n,dt1);
	WallColl(&vx,&vy,n1,n2);
	dt1 = dt-TimeWallColl; //??
      }
      //if the next collision is between particles
      else if (TimePartColl<=dt1 && TimePartColl<TimeWallColl){
	dt1 = TimePartColl;
	dynamics(&x,&y,vx,vy,n,dt1);
	ParticleColl(&vx,&vy,angle,n3,n4);
	dt1 = dt-TimePartColl;

      }
      else{
	dt1=TimePartColl; //?? sao iguais e tanto faz??
	dynamics(&x,&y,vx,vy,n,dt1);
	WallColl(&vx,&vy,n1,n2);
	ParticleColl(&vx,&vy,angle,n3,n4);
	dt1 = dt-TimePartColl;

      }


      //cout << t << "\n";
    
    }
    //cout << "batata2";


    //velocity stats
    float vel = 0;
    for(int j = 0;j<iterations;j++){
      for(int k = 0;k<n;k++){
	if(maxvelocity*maxvelocity< vmean[j][k]){
	  maxvelocity = vmean[j][k];
	}
      } 
    }
    bins = (int)((float)n/7.0)+1;
    float dvel = maxvelocity/(float)bins;
    meanvelocity.resize(iterations,vector<float>(bins,0));

    for(int j = 0;j<iterations;j++){
      for(int k = 0;k<n;k++){
	vel=vmean[j][k];
	meanvelocity[j][(int)(vel/dvel)]+=1;
      }
    }
   
     for(int j = 0;j<iterations;j++){
      for(int k = 0;k<bins;k++){
	if(meanvelocity[j][k]>maxelement){
	  maxelement = meanvelocity[j][k];
	}
      }
    }
   

    glutInit(& argc, argv);

    glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);
  
    glutInitDisplayMode(GLUT_DOUBLE);
    glutInitWindowSize(windowWidth,windowHeight);
    glutInitWindowPosition(windowPosX, windowPosY);
    glutCreateWindow(title);

    glutDisplayFunc(display);
    glutKeyboardFunc(key);
    glutSpecialFunc(specialKeys);
    glutReshapeFunc(reshape);
    glutTimerFunc(0,Timer,0);

    

    initGL();
    glutMainLoop();
    cout << "Quit? (y) or (n)\n";
    std::string quit (1, 'y');

    jump:
    cin >> quit;

    bool quit_flag;
    if(quit == "y")
      break;
    else if (quit == "n")
      continue;
    else{
      cout << "y or n?\n";
      goto jump;
    }
    
  }
  
  return 0;
			   
}
